use unitage::*;
use num::Float;
use std::f64;

#[test]
fn test_basic_usage() {
    let btu: PhysicalQuantity<f64, _> = pq!(1f64, "Btu");
    let j = unit!("J").value(btu);
    let btu2j = 4.1868 * 453.59237 * 5.0 / 9.0;
    let diff = 1.0 - j / btu2j;

    assert!(diff < f64::EPSILON,
        "{} >= {}", diff, f64::EPSILON
    );

    let j = j.integer_decode();
    let btu2j = btu2j.integer_decode();

    assert_eq!(j.2, btu2j.2);
    assert_eq!(j.1, btu2j.1);
    assert!(j.0 ^ btu2j.0 <= 1);
}

#[test]
fn test_const() {
    const CONST: PhysicalQuantity<f64, predefined::dim::Pressure> = pq!(1f64, "ata", f64);

    assert_eq!(unit!("psi").value(CONST), 14.223343307119562);
}


#[test]
fn test_high_code_char() {
    const CONST: PhysicalQuantity<f64, predefined::dim::Dimensionless> = pq!(1f64, "%", f64);

    let u = "\u{2014}".parse::<Unit<_, _>>().unwrap();

    assert_eq!(u.value(CONST).unwrap(), 0.01);
}

#[test]
fn test_rad() {
    let deg = pq!(180f64, "°");
    let u = unit!("rad");
    let rad = u.value(deg);
    let left = rad.integer_decode();
    let right = f64::consts::PI.integer_decode();

    assert_eq!(rad, f64::consts::PI, "\n{:b}, {}\n{:b}, {}\n", left.0, left.1, right.0, right.1);
}