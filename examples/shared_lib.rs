use unitage::*;
use std:: {
    cmp::min,
    error::Error,
    ffi::CStr,
    panic::catch_unwind,
};

#[repr(C)]
pub enum ParamType {
    Double = 0,
    String = 1,
    DoubleArr = 2,
    StringArr = 3,
    CellArr = 4,
    None = 5,
}

#[no_mangle]
pub extern "system" fn convert_to(out_unit: *const u8, in_value: f64, in_unit: *const u8) -> f64 {
    match catch_unwind(|| impl_convert_to(out_unit, in_value, in_unit)) {
        Ok(result) => match result {
            Ok(d) => d,
            Err(_) => f64::NEG_INFINITY,
        },
        Err(_) => f64::NAN,
    }
}

#[no_mangle]
pub extern "system" fn sub_convert_to(out_value: &mut f64, out_unit: *const u8, in_value: &f64, in_unit: *const u8) {
    *out_value = match catch_unwind(|| impl_convert_to(out_unit, *in_value, in_unit)) {
        Ok(result) => match result {
            Ok(d) => d,
            Err(_) => f64::NEG_INFINITY,
        },
        Err(_) => f64::NAN,
    };
}

fn impl_convert_to(out_unit: *const u8, in_value: f64, in_unit: *const u8) -> Result<f64, Box<dyn Error>> {
    let ou = unsafe { CStr::from_ptr(out_unit as *const i8) }.to_str()?;
    let iu = unsafe { CStr::from_ptr(in_unit as *const i8) }.to_str()?;
    let iunit: Unit<f64, _> = iu.parse()?;
    let ounit: Unit<f64, _> = ou.parse()?;
    let pq = iunit.pq(in_value);

    ounit.value(pq).map_err(|e| e.into())
}

#[no_mangle]
pub extern "system" fn GetFunctionCount(count: &mut u16) {
    *count = 1;
}

#[no_mangle]
pub extern "system" fn GetFunctionData(
    fn_no: &u16,
    fn_name: &mut[u8; 256],
    n_param: &mut u16,
    types: &mut[ParamType; 16],
    name: &mut[u8; 256])
{
    match fn_no {
        0 => {
            copy_string("sub_convert_to", &mut fn_name[..]);
            *n_param = 4;
            types[0] = ParamType::Double;
            types[1] = ParamType::String;
            types[2] = ParamType::Double;
            types[3] = ParamType::String;
            copy_string("sub_convert_to", &mut name[..]);
        },
        _ => unreachable!(),
    }
}

#[no_mangle]
pub extern "system" fn GetParameterDescription(
    fn_no: &u16,
    arg_no: &u16,
    p_name: &mut[u8; 256],
    p_desc: &mut[u8; 256])
{
    match fn_no {
        0 => {
            match arg_no {
                1 => copy_string("output unit", &mut p_name[..]),
                2 => copy_string("input value", &mut p_name[..]),
                3 => copy_string("input unit", &mut p_name[..]),
                _ => (),
            }
        },
        _ => unreachable!(),
    }
    copy_string("unit converter", &mut p_desc[..]);
}

fn copy_string(src: &str, dest: &mut[u8]) {
    let n = min(src.len(), dest.len() - 1);

    dest.copy_from_slice(&src.as_bytes()[..n]);
    dest[n] = 0;
}