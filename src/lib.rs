//! [![Latest Release](https://gitlab.com/fton/unitage/-/badges/release.svg)](https://gitlab.com/fton/unitage/-/releases)
//! [![pipeline status](https://gitlab.com/fton/unitage/badges/main/pipeline.svg)](https://gitlab.com/fton/unitage/-/commits/main)
//! [![coverage report](https://gitlab.com/fton/unitage/badges/main/coverage.svg)](https://gitlab.com/fton/unitage/-/commits/main)
//! 
//! # Description
//! Yet one more another unit/dimension support library for rust.
//!
//! This package lets you write units of physical quantities in the same way you always write it.
//! 
//! # Usage
//! ```rust
//! use ::unitage:: { *, predefined::* };
//!
//! fn bmi(hight: Length<f64>, weight: Mass<f64>) -> f64 {
//!     let bmi = weight / hight / hight;
//!
//!     unit!("kg/m2").value(bmi)
//! }
//!
//! let h = pq!(5f64, "ft.") + pq!(8f64, "in.");
//! let w = pq!(200f64, "lb");
//!
//! assert_eq!(bmi(h, w), 30.409583894427303); // Lose weight!!
//! ```
//!  
//! # Package Structure
//! This package is a entry points for each implementation packages.
//!
//! For details following topics, please refer the documentation of each sub-package.
//! - [Syntax of Unit String](https://docs.rs/physical-quantity/*/physical_quantity/unit/struct.Parser.html)
//! - [Default Predefined Unit Definitions](https://docs.rs/physical-quantity/*/physical_quantity/predefined/unit/index.html)
#![no_std]
pub use physical_quantity:: {
    self,
    Dimension, Dim, DynDim, PhysicalQuantity, Unit, Conv,
    predefined,
    traits,
};
pub use const_frac:: { self, Frac, Real };
pub use typenum;

pub use unit_proc;
pub use real_proc;

/// Generate [Frac] from decimal numerical literal.
/// When the argument is illegal, compile will fail.
#[macro_export]
macro_rules! frac {
    ($value:literal) => {
        $crate::real_proc::frac!($crate, $value)
    };
}

/// Generate [Dim] from unit string.
/// ```
/// use unitage:: { dim, PhysicalQuantity };
///
/// let height: PhysicalQuantity<f64, dim!("cm")> = PhysicalQuantity { value: 172.0, dim: Default::default() };
/// ```
/// When the unit string is illegal, compile will fail.
#[macro_export]
macro_rules! dim {
    ($value:literal) => {
        $crate::unit_proc::dim!($crate, $value)
    };
}

/// Generate [Unit] from unit string.
/// ```
/// use unitage::unit;
///
/// assert_eq!(unit!("lb").pq(200f64), unit!("kg").pq(90.718474));
/// ```
/// When the unit string is illegal, compile will fail.
#[macro_export]
macro_rules! unit {
    ($unit:literal) => {
        $crate::unit_proc::unit!($crate, $unit)
    };
    ($unit:literal, $init:path) => {
        $crate::unit_proc::unit!($crate, $unit, $init)
    };
}

/// Generate [PhysicalQuantity] from value and unit string.
/// ```
/// use unitage:: { pq, unit };
///
/// let speed = pq!(100f64, "km/h");
///
/// assert_eq!(unit!("mile/h").value(speed), 62.137119223733397);
/// ```
/// If you need [PhysicalQuantity] in const context, give the additional argument
/// for initializing real type (generic type parameter `R`).
/// For [f64] and [Frac],
/// you can specify the predefined initializer `f64` and `Frac` respectively.
/// ```
/// use unitage:: { pq, unit, PhysicalQuantity, predefined::dim };
///
/// const LIGHT_YEAR: PhysicalQuantity<f64, dim::Length> = pq!(1f64, "c year", f64);
///
/// assert_eq!(unit!("km").value(LIGHT_YEAR), 	9_460_730_472_580.8);
/// ```
/// When the unit string is illegal, compile will fail.
#[macro_export]
macro_rules! pq {
    ($value:expr, $unit:literal) => {
        $crate::unit_proc::unit!($crate, $unit).pq($value)
    };
    ($value:expr, $unit:literal, Frac) => {{
        let u = $crate::unit_proc::unit!($crate, $unit, $crate::Frac);

        $crate::PhysicalQuantity {
            value: u.a.mul($value).add(u.b),
            dim: u.dim,
        }
    }};
    ($value:expr, $unit:literal, f64) => {{
        let u = $crate::unit_proc::unit!($crate, $unit, ::core::primitive::f64);

        $crate::PhysicalQuantity {
            value: u.a * $value + u.b,
            dim: u.dim,
        }
    }};
    ($value:expr, $unit:literal, $init:path) => {{
        let u = $crate::unit_proc::unit!($crate, $unit, $init);

        $crate::PhysicalQuantity {
            value: u.a * $value + u.b,
            dim: u.dim,
        }
    }};
}