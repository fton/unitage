[![Latest Release](https://gitlab.com/fton/unitage/-/badges/release.svg)](https://gitlab.com/fton/unitage/-/releases)
[![pipeline status](https://gitlab.com/fton/unitage/badges/main/pipeline.svg)](https://gitlab.com/fton/unitage/-/commits/main)
[![coverage report](https://gitlab.com/fton/unitage/badges/main/coverage.svg)](https://gitlab.com/fton/unitage/-/commits/main)
# Description
Yet one more another unit/dimension support library for rust.

This package lets you write units of physical quantities in the same way you always write it.
# Example
There is Libreoffice Calc example in this package repository.

![example](https://gitlab.com/fton/unitage/uploads/a412719447609572cf51f5dc2926ba7c/example.gif)

# Project status
This package is very early stage.<br>
Far from stable, almost no tested, API will change frequently.
# Package Structure
This package is a entry points for each implementation packages.

For details following topics, please refer the documentation of each sub-package.
- [API of this package](https://docs.rs/unitage/*/unitage/index.html)
- [Syntax of Unit String](https://docs.rs/physical-quantity/*/physical_quantity/unit/struct.Parser.html)
- [Default Predefined Unit Definitions](https://docs.rs/physical-quantity/*/physical_quantity/predefined/unit/index.html)